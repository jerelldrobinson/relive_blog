var express = require('express');
var router = express.Router();

var passport = require('passport');
var mongoose = require('mongoose');
var jwt = require('express-jwt');

var auth = jwt({secret: 'SECRET', userProperty: 'payload'});

var nodemailer = require('nodemailer');

var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');
var User = mongoose.model('User');
var Vote = mongoose.model('Vote');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

var getUser = function(req, res, next){
  auth(req, res, function(){
    User.find({username: req.payload.username}, function (err, users){
      if (err) { return next(err); }
      if (!users) { return next(new Error('can\'t find user')); }
      if (users.length > 1) {return next(new Error('Error!  Multiple users with the same name!'))}
      req.currentUser =  users[0];

      return next();
    });
  });
}



//user route section

router.get('/admin', function(req, res, next) {
	User.find(function(err, users) {
		if(err) {return next(err); }

		res.json(users);
	});
});

router.post('/admin/:user/delete',  function(req, res, next) {
	User.find(function(err, users) {
		if(err) {return next(err); }

		res.json(users);
	});
});
//preloading post objects
router.param('user', function(req,res, next, id) {
	var query = User.findById(id);

	query.exec(function(err, user) {
		if(err) {return next(err); }
		if (!user) {return next(new Error('can\'t find user')); }

		req.user = user;
		return next();
	});
});

//return single user
/*router.get('/users/:user', function(req, res, next) {
	//sanitize user
	var user = new User();
	user.username = req.user.username;
	user.role = req.user.role;
	user.selected = req.user.selected;
	user._id = req.user._id;
	//console.log(user);
	res.json(user);
});*/

router.get('/users/:user', function(req, res, next) {
	req.user.populate('user', function(err, user) {
		if(err) {return next(err); }

		res.json(user);
	});
});

router.get('/posts/:post', function(req, res, next) {
	req.post.populate('comments', function(err, post) {
		if(err) {return next(err); }

		res.json(post);
	});
});

// votes endpoint
router.post('/posts/:user/votes',  function(req, res, next) {
	Vote.find(function(err, votes) {
		if(err) {return next(err); }

		res.json(votes);
	});
});
//

//post route section
router.get('/posts',  function(req, res, next) {
	Post.find(function(err, posts) {
		if(err) {return next(err); }

		res.json(posts);
	});
});

router.post('/posts',auth, function(req, res, next) {
	var post = new Post(req.body);

	post.author = req.payload.username;

	post.save(function(err, post) {
		if(err) {return next(err); }

		res.json(post);
	});
});



//preloading post objects
router.param('post', function(req,res, next, id) {
	var query = Post.findById(id);

	query.exec(function(err, post) {
		if(err) {return next(err); }
		if (!post) {return next(new Error('can\'t find post')); }

		req.post = post;
		return next();
	});
});

//return single post
router.get('/posts/:post', function(req, res, next) {
	req.post.populate('comments', function(err, post) {
		if(err) {return next(err); }

		res.json(post);
	});
});

//upvote endpoint
router.put('/posts/:post/upvote',auth, function(req, res, next) {
	req.post.upvote(req.payload.username,function(err, post) {
		if(err) {return next(err); }

		res.json(post);
	});
});

//downvote endpoint
router.put('/posts/:post/downvote',auth, function(req, res, next) {
	req.post.downvote(req.payload.username,function(err, post) {
		if(err) {return next(err); }

		res.json(post);
	});
});

router.put('/posts/:post/comments/:comment/upvote',auth, function(req, res, next) {
	req.comment.upvote(req.payload.username,function(err, comment) {
		if(err) {return next(err); }

		res.json(comment); 
	});
});
//downvote comment endpoint
router.put('/posts/:post/comments/:comment/downvote',auth, function(req, res, next) {
	req.comment.downvote(req.payload.username,function(err, comment) {
		if(err) {return next(err); }

		res.json(comment); 
	});
});
//comment endpoint
router.post('/posts/:post/comments', auth, function(req, res, next) {
	var comment = new Comment(req.body);
	comment.post = req.post;
	comment.author = req.payload.username;
	

	comment.save(function(err, comment) {
		if(err){return next(err); }

		req.post.comments.push(comment);
		req.post.save(function(err, post) {
			if(err){return next(err); }

			res.json(comment);
		});
	});
});

router.param('comment', function(req, res, next, id) {
	var query = Comment.findById(id);

	query.exec(function (err, comment) {
		if(err) {return next(err); }
		if (!comment) {return next(new Error('can\'t find comment')); }

		req.comment = comment;
		return next();
	});
});
//login endpoints
router.post('/register', function(req, res, next) {
	if(!req.body.username || !req.body.password) {
		return res.status(400).json({message: 'Please fill out all fields'});
	}

	var user = new User();

	user.username = req.body.username;

	user.setPassword(req.body.password);

	user.role = 0;

	user.save(function (err) {
		if(err) {return next(err); }

		return res.json({token: user.generateJWT()});
	});
});

router.post('/login', function(req, res, next) {
	if(!req.body.username || !req.body.password) {
		return res.status(400).json({message: 'Please fill out all fields'});
	}
	passport.authenticate('local', function(err, user, info) {
		if(err) {return next(err); }

		if(user) {
			return res.json({token: user.generateJWT() });
		} else {
			return res.status(401).json(info);
		}
	})(req, res, next);
});
module.exports = router;


