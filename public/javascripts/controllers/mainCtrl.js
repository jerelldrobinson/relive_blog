app.controller('MainCtrl', [
	'$scope',
	'posts',
	'auth',
	function($scope, posts, auth){
		// posts model
		$scope.posts = posts.posts;

		$scope.isLoggedIn = auth.isLoggedIn;

		// add posts method
		$scope.addPost = function() {
			if(!$scope.title || $scope.title === ''){return; }
			posts.create({
				title: $scope.title,
				link: $scope.link,

			});		
			$scope.title = '';
			$scope.link = '';
		};

		$scope.incrementUpvotes = function(post) {
			posts.upvote(post);
		};
		$scope.decrementUpvotes = function(post) {
			posts.downvote(post);
		};
	}]);

