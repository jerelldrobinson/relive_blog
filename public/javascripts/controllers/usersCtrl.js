app.controller('UsersCtrl', [
	'$scope',
	'users',
	'auth',
	function($scope, 
		users, 
		auth){

		$scope.users = users.users;

		$scope.isLoggedIn = auth.isLoggedIn;

		$scope.options = [
		{label: ' ... ', value: 0},
		{label: 'user', value: 1 },
		{label: 'admin', value: 2}
		];
		
		//public helper methods
		$scope.ucFirst = function(value){
			if (value === undefined){ return value; }
    		return value.charAt(0).toUpperCase() + value.slice(1);
		};
		// add posts method
		$scope.addUser = function() {
			if(!$scope.username || $scope.password === ''){return; }
			users.create({
				username: $scope.username,
				salt: $scope.salt,
				hash: $scope.hash,

			});
	
			$scope.username = '';
		};

		$scope.setSelected = function(user) {
			$scope.user = user;
			$scope.profName = user.username;
			console.log('selected user: ' + $scope.user.username);
		};
	}]);

