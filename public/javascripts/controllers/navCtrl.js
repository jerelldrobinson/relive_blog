app.controller('NavCtrl', [
	'$scope',
	'auth',
	'$location',
	function($scope, auth, $location) {
		$scope.isLoggedIn = auth.isLoggedIn;
		$scope.currentUser = auth.currentUser;
		$scope.logOut = auth.logOut;

		// navbar active inactive function
		$scope.isActive = function(viewLocation) {
			//console.log('viewLocation: ' + viewLocation);
			//console.log('viewLocation: ' + $location.path());
			return viewLocation === $location.path();
		};

		$scope.ucFirst = function(value){
			if (value === undefined){ return value; }
    		return value.charAt(0).toUpperCase() + value.slice(1);
		};


	}]);


