app.directive('tab', function() {
	return {
		restrict: 'E',
		transclude: true,
		templateUrl: 'profile.html',
		scope: { }
	};
});