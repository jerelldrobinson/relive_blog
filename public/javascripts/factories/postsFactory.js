// post service
app.factory('posts', ['$http', 'auth',function($http, auth) {
	var o = {
		posts: []
	};
	o.get = function(id) {
		return $http.get('/posts/' + id).then(function(res) {
			return res.data;
		});
	};
	o.getAll = function() {
		return $http.get('/posts').success(function(data) {
			angular.copy(data, o.posts);
		});
	};
	o.create = function(post) {
		return $http.post('/posts', post, {
			headers: {Authorization: 'Bearer ' +auth.getToken()}
		}).success(function(data) {
			o.posts.push(data);
		});
	};
	o.addComment = function(id, comment) {
		return $http.post('/posts/' + id + '/comments', comment, {
			headers: {Authorization: 'Bearer ' +auth.getToken()}
		});
	};
	o.upvote = function(post) {
		return $http.put('/posts/' + post._id + '/upvote', null, {
			headers: {Authorization: 'Bearer ' +auth.getToken()}
		}).success(function(data) {
			//$scope.post = data;
			console.log("post upvote" + data);
		});
	};
	o.downvote = function(post) {
		return $http.put('/posts/' + post._id + '/downvote', null, {
			headers: {Authorization: 'Bearer ' +auth.getToken()}
		}).success(function(data) {
			//post = data;
			console.log("post downvote" + data);
		});
	};
	o.upvoteComment = function(post, comment) {
		return $http.put('/posts/' + post._id + '/comments/' + comment._id + '/upvote', null, {
			headers: {Authorization: 'Bearer ' +auth.getToken()}
		}).success(function(data) {
			//comment.count = data.count;
			console.log("comment upvote"+data);
		})
		.error(function (data, status, headers, config) {
			console.log('status: >>>' + status + '\n config ' + config);
			console.log('Error reading JSON file. - ' + data);
		}); 
	};
	o.downvoteComment = function(post, comment) {
		return $http.put('/posts/' + post._id + '/comments/' + comment._id + '/downvote', null, {
			headers: {Authorization: 'Bearer ' +auth.getToken()}
		}).success(function(data) {
			//comment.count = comment.getAggregateScore();
			console.log("comment downvote"+data);	
		})
		.error(function (data, status, headers, config) {
			console.log('status: >>>' + status + '\n config ' + config);
			console.log('Error reading JSON file. - ' + data);
		}); 
	};
	return o;
}]);

