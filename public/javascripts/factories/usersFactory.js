app.factory('users', ['$http', 'auth', function($http, auth) {

	
	var u = {
		users: []
	};

	u.get = function(id) {
		return $http.get('/users/' + id).then(function(res) {
			return res.data;
		});
	};

	u.getAll = function() {
		return $http.get('/admin').success(function(data) {
			angular.copy(data, u.users);
		})
		.success(function(data) {
		})
		.error(function (data, status, headers, config) {
			console.log('status: >>>' + status + '\n header: {}' + headers + ' config ' + config);
			console.log('Error reading JSON file. - ' + data);
		});
	};
	u.create = function(user) {
		return $http.post('/admin', user, {
			headers: {Authorization: 'Bearer ' +auth.getToken()}
		}).success(function(data) {
			u.users.push(data);
		});
	};
	u.delete = function(id) {
		return $http.post('/admin/' + id + '/delete', {
			headers: {Authorization: 'Bearer ' +auth.getToken()}
		});
	};
	return u;
}]);

