app.filter('searchFilter', function() {
	return function(items, name) {
		var filtered = [];
		if (name === undefined) {
			return items;
		}
		var rgx = new RegExp(name, 'i');

		angular.forEach(items, function(item) {
			var match = rgx.test(item.username);

			if (match) {
				filtered.push(item);

			}
		});
		return filtered;
	};
});