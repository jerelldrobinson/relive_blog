var app = angular.module('reliveBlog', ['ui.router']);

//begin service

var getUser = function(req, res, next){
  auth(req, res, function(){
    User.find({username: req.payload.username}, function (err, users){
      if (err) { return next(err); }
      if (!users) { return next(new Error('can\'t find user')); }
      if (users.length > 1) {return next(new Error('Error!  Multiple users with the same name!'))}
      req.currentUser =  users[0];

      return next();
    });
  });
}

/**
 * A convenient method to use if you want to retrieve the logged in user but 
 * being logged in isn't necessary to complete the action
 */
var getUserOptional = function(req, res, next){
  if (req.get("Authorization")) {
    return getUser(req, res, next);
  }
  else {
    return next();
  }
}


