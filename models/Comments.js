var mongoose = require('mongoose');

var CommentSchema = new mongoose.Schema({
	body: String,
	author: String,
	upvotes: [{ type: String}],
	downvotes: [{ type: String}],
	count: {type:Number, default:0},
	posts: {type: mongoose.Schema.Types.ObjectId, ref: 'Post' },
});

CommentSchema.methods.upvote = function(cb) {
	if (!this.alreadyUpVoted(cb) && !this.alreadyDownvoted(cb)) {
		console.log('user with id ' + cb + ' has not upvoted' );
		this.upvotes.push(cb);
		this.count = this.getAggregateScore();
		this.save();
	} else if (!this.alreadyUpVoted(cb) && this.alreadyDownvoted(cb)) {
		console.log('user with id ' + cb + ' canceled downvote' );
		this.removeDownVote(cb);
		this.count = this.getAggregateScore();
		this.save();
	}
	
};

CommentSchema.methods.downvote = function(cb) {
	if (!this.alreadyDownvoted(cb) && !this.alreadyUpVoted(cb)) {
		console.log('user with id ' + cb + ' has not downvoted' );
		this.downvotes.push(cb);
		this.count = this.getAggregateScore();
		this.save();
	}else if (!this.alreadyDownvoted(cb) && this.alreadyUpVoted(cb)) {
		console.log('user with id ' + cb + ' canceled upvote' );
		this.removeUpVote(cb);
		this.count = this.getAggregateScore();
		this.save();
	}
	
};

CommentSchema.methods.removeDownVote = function(name) {
	this.downvotes = this.downvotes
               .filter(function (el) {
                        return el.name === name;
                       });
};

CommentSchema.methods.removeUpVote = function(name) {
	this.upvotes = this.upvotes
               .filter(function (el) {
                        return el.name === name;
                       });
};


CommentSchema.methods.alreadyDownvoted = function(id) {

	var isInArray = this.downvotes.some(function (vote) {
		console.log('match found: ' + (id === vote));
    	return (id === vote);
	});
	return isInArray;
};

CommentSchema.methods.alreadyUpVoted = function(id) {

	var isInArray = this.upvotes.some(function (vote) {
		console.log('match found: ' + (id === vote));
    	return (id === vote);
	});
	return isInArray;
};
CommentSchema.methods.getAggregateScore = function() {
	return this.upvotes.length - this.downvotes.length;
};


mongoose.model('Comment', CommentSchema);