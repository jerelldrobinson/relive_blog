var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
	title: String,
	link: String,
	count: {type:Number, default:0},
	upvotes: [{ type: String}],
	downvotes: [{ type: String}],
	comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}]
});

PostSchema.methods.upvote = function(cb) {
	if (!this.alreadyUpVoted(cb) && !this.alreadyDownvoted(cb)) {
		console.log('user with id ' + cb + ' has not upvoted' );
		this.upvotes.push(cb);
		this.count = this.getAggregateScore();
		this.save();
	} else if (!this.alreadyUpVoted(cb) && this.alreadyDownvoted(cb)) {
		console.log('user with id ' + cb + ' canceled downvote' );
		this.removeDownVote(cb);
		this.count = this.getAggregateScore();
		this.save();
	}
	
};

PostSchema.methods.downvote = function(cb) {
	if (!this.alreadyDownvoted(cb) && !this.alreadyUpVoted(cb)) {
		console.log('user with id ' + cb + ' has not downvoted' );
		this.downvotes.push(cb);
		this.count = this.getAggregateScore();
		this.save();
	}else if (!this.alreadyDownvoted(cb) && this.alreadyUpVoted(cb)) {
		console.log('user with id ' + cb + ' canceled upvote' );
		this.removeUpVote(cb);
		this.count = this.getAggregateScore();
		this.save();
	}
	
};

PostSchema.methods.removeDownVote = function(name) {
	this.downvotes = this.downvotes
               .filter(function (el) {
                        return el.name === name;
                       });
};

PostSchema.methods.removeUpVote = function(name) {
	this.upvotes = this.upvotes
               .filter(function (el) {
                        return el.name === name;
                       });
};

PostSchema.methods.alreadyDownvoted = function(id) {

	var isInArray = this.downvotes.some(function (vote) {
		console.log('match found: ' + (id === vote));
    	return (id === vote);
	});
	return isInArray;
};

PostSchema.methods.alreadyUpVoted = function(id) {

	var isInArray = this.upvotes.some(function (vote) {
		console.log('match found: ' + (id === vote));
    	return (id === vote);
	});
	return isInArray;
};
PostSchema.methods.getAggregateScore = function() {
	return this.upvotes.length - this.downvotes.length;
};



mongoose.model('Post', PostSchema);