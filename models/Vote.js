var mongoose = require('mongoose');

var VoteSchema = new mongoose.Schema({
	user: {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
	post: {type: mongoose.Schema.Types.ObjectId, ref: 'Post' },
	comment: {type: mongoose.Schema.Types.ObjectId, ref: 'Comment'},
	// value is +1 for upvote, -1 for downvote
   // or 0 if user cancels their vote
	value: {type: Number, default: 0},
});

VoteSchema.methods.upvote = function(cb) {
/*	this.upvotes += 1;
	this.save(cb);*/
};

VoteSchema.methods.downvote = function(cb) {
/*	this.upvotes -= 1;
	this.save(cb);*/
};

mongoose.model('Vote', VoteSchema);