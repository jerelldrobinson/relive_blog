//begin app config
app.config([
	'$stateProvider',
	'$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {

		$stateProvider
		.state('home', {
			url: '/home',
			templateUrl: 'home.html',
			controller: 'MainCtrl',
			resolve: {
				postPromise: ['posts', function(posts) {
					return posts.getAll();
				}]
			}
		})
		.state('posts', {
			url: '/posts/{id}',
			templateUrl: 'posts.html',
			controller: 'PostsCtrl',
			resolve: {
				post: ['$stateParams', 'posts', function($stateParams, posts) {
					return posts.get($stateParams.id);
				}]
			}
		})
		.state('login', {
			url: '/login',
			templateUrl: 'login.html',
			controller: 'AuthCtrl',
			onEnter: ['$state', 'auth', function($state, auth) {

				if(auth.isLoggedIn()){
					$state.go('home');
				}
			}]
		})
		.state('register', {
			url: '/register',
			templateUrl: 'register.html',
			controller: 'AuthCtrl',
			onEnter: ['$state', 'auth', function($state, auth) {

				if(auth.isLoggedIn()){
					$state.go('home');
				}
			}]
		})
		.state('admin', {
			url: '/admin',
			templateUrl: 'manage_users.html',
			controller: 'UsersCtrl',
			resolve: {
				userPromise: ['users', function(users) {
					return users.getAll();
				}]
			}
		})
		.state('users', {
			url: '/users/{id}',
			templateUrl: 'user.html',
			controller: 'UsersCtrl',
			resolve: {
				user: ['$stateParams', 'users', function($stateParams, users) {
					return users.get($stateParams.id);
				}]
			}
		});
		$urlRouterProvider.otherwise('home');
	}]);
//end app config
