#Installation and Configuration using Sublime Text Editor

### Required downloads
1. TODO
2. TODO
3. TODO
4. [Preferred text editr [sublimne](http://www.sublimetext.com/)
5. [Git](http://git-scm.com/)

### Configuration
####1. make sure java and play enviroment variables are set

####2. fork the repository to your github account

####3. clone the repository
    git clone https://bitbucket.org/jerelldrobinson/relive_blog.git

####5. clean and compile the project
    TODO

### Setting up Sublime
1. TODO
2. TODO
3. TODO
4. TODO
5. TODO
6. TODO



# Git

### Common commands during development:

#### Create a branch to work on:

#####1. features:
    git checkout development
    git checkout -b feature-issue#-featureName

#####2. bugs:
    git checkout development
    git checkout -b bug-issue#-bugName


#### Sync development branch (fork) with development branch (main repository):

#####1. make sure you have a remote pointing upstream:
    git remote

#####2. if you don't, add one:
    git remote add upstream https://bitbucket.org/jerelldrobinson/relive_blog.git

#####3. sync:
    git checkout development
    git pull upstream development
    git push origin development


#### Prepare to send a pull request:

    git checkout development
    git pull upstream development
    git checkout branchName
    git rebase development

Rebasing will update your branch with the development branch.
You can find more info on rebasing [here](http://git-scm.com/book/ch3-6.html).

#### Sending a Pull Request from GitHub

Information on doing pull requests can be found [here](https://help.github.com/articles/using-pull-requests).

#### If your Pull Request is Rejected

    git checkout development
    git pull upstream development
    git checkout branchName
    git rebase development
    ~~~fix issues~~~
    git checkout development
    git pull upstream development
    git checkout branchName
    git rebase development
    git push origin branchName

#### Deleting your branch:

    git branch -d branchName
    git push origin :branchName
    
//borrowed from https://github.com/FEMR/femr/blob/master/CONTRIBUTING.md